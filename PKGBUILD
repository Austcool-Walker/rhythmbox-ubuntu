# Maintainer: Jan de Groot <jgc@archlinux.org>
# Contributor: Giovanni Scafora <giovanni@archlinux.org>

pkgname=rhythmbox-ubuntu
pkgver=3.4.3
pkgrel=1
pkgdesc="Music playback and management application"
arch=(x86_64)
license=(GPL)
url="https://launchpad.net/ubuntu/+source/rhythmbox"
depends=(dconf gst-plugins-base gst-plugins-good libsoup json-glib libnotify libpeas
         media-player-info totem-plparser tdb python-gobject libgudev grilo)
makedepends=(itstool intltool brasero gobject-introspection vala grilo libdmapsharing lirc libgpod
             libmtp gtk-doc yelp-tools git)
checkdepends=(check xorg-server-xvfb)
optdepends=('gst-plugins-ugly: Extra media codecs'
            'gst-plugins-bad: Extra media codecs'
            'gst-libav: Extra media codecs'
            'brasero: Audio CD Recorder plugin'
            'libdmapsharing: DAAP Music Sharing plugin'
            'grilo-plugins: Grilo media browser plugin'
            'lirc: LIRC plugin'
            'libgpod: Portable Players - iPod plugin'
            'libmtp: Portable Players - MTP plugin'
            'gvfs-mtp: Portable Players - Android plugin')
options=('!emptydirs')
provides=('rhythmbox' 'rhythmbox-git' 'rhythmbox-ubuntu')
conflicts=('rhythmbox' 'rhythmbox-git')
replaces=('rhythmbox' 'rhythmbox-git')
source=("http://archive.ubuntu.com/ubuntu/pool/main/r/rhythmbox/rhythmbox_${pkgver}.orig.tar.xz")
sha512sums=('SKIP')

prepare() {
  cd rhythmbox-${pkgver}
    for i in $(grep -v '#' ${BUILDDIR}/debian/patches/series); do
        patch -p1 -i "${BUILDDIR}/debian/patches/${i}"
    done
  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd rhythmbox-${pkgver}
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --libexecdir=/usr/lib/rhythmbox \
    --disable-browser-plugin \
    --disable-static \
    --enable-daap \
    --enable-gtk-doc \
    --enable-python \
    --enable-vala \

  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

check() {
  cd rhythmbox-${pkgver}
  glib-compile-schemas --targetdir=tests --schema-file=data/org.gnome.rhythmbox.gschema.xml
  GSETTINGS_BACKEND=memory \
  GSETTINGS_SCHEMA_DIR="$PWD/tests" \
  CK_TIMEOUT_MULTIPLIER=3 \
  xvfb-run -a -n 63 make check
}

package() {
  cd rhythmbox-${pkgver}
  make DESTDIR="$pkgdir" install
  rm -r "$pkgdir/usr/lib/rhythmbox/sample-plugins"
  rm -r "$pkgdir/usr/lib/rhythmbox/plugins/rbzeitgeist"
}
